import webintr

def f(a,b):
    return a+b

webintr.register(f,[('a','int'),
                    ('b','int')],
                 'int')

webintr.run()

