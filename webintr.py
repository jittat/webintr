from flask import Flask
from flask import request

app = Flask(__name__)

intr_config = {
    'function': None,
    'inputs': [],
    'output_type': 'int',
    'output_log': []
}

def register(f, inputs, output_type='int'):
    intr_config['function'] = f
    intr_config['inputs'] = inputs
    intr_config['output_type'] = output_type

def convert_input(raw, input_type):
    if input_type == 'int':
        try:
            return int(raw)
        except:
            return 0
    
def append_log(form):
    f = intr_config['function']
    args = {}
    for var, t in intr_config['inputs']:
        args[var] = convert_input(form.get(var,''), t)
    output = f(**args)
    intr_config['output_log'].append('ARGS: ' + str(args) +
                                     ' OUTPUT = ' + str(output))
    print(intr_config['output_log'])
    
@app.route("/", methods=['GET','POST'])
def form():
    if request.method == 'POST':
        append_log(request.form)
    items = []
    items.append('<form method="post">')
    for var, t in intr_config['inputs']:
        items.append(var + ': <input name="' + var + '"/><br />')
    items.append('<input type="submit" />')
    items.append('</form>')
    items.append('<hr />')
    items.append('Logs<br />')
    items += [l + '<br />' for l in reversed(intr_config['output_log'])]
    return ''.join(items)

def run():
    app.run(debug=True)
